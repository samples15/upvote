import { createStore } from 'vuex';

export default createStore({
  state: {
    voteList: [
      {
        id: 1,
        votes: 3,
        selected: false,
      },
      {
        id: 2,
        votes: 2,
        selected: false,
      },
      {
        id: 3,
        votes: 5,
        selected: false,
      },
    ],
  },
  mutations: {
    setVote(state, id) {
      state.voteList.find((item) => item.id === id).votes += 1;
    },
    setState(state, id) {
      const initialState = state.voteList.find((item) => item.id === id).selected;
      state.voteList.find((item) => item.id === id).selected = !initialState;
    },
  },
  actions: {
    requestAddVote({ commit }, id) {
      commit('setVote', id);
    },
    requestUpdateState({ commit }, obj) {
      commit('setState', obj);
    },
  },
});
